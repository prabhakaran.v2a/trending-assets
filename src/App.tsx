import React from 'react';
import './App.css';
import TrendingAssets from './components/TrendingAssets';

function App() {
  return (
    <div className="App">
      
      <TrendingAssets />
    </div>
  );
}

export default App;
