import React, {useEffect} from 'react';
import axios from "axios";

const Cards = ({
    icon,
    name,
    price,
    percentage,
    lockedValue,
    pairings,
    iconLink,
    changeAsset,
    assestIcon
  }: {
    icon?: any;
    name?: string;
    price?: any;
    percentage?: any;
    lockedValue?: any;
    iconLink?:any;
    pairings?: any;
    changeAsset?: string;
    assestIcon?: any;
  }) => {
    return (
    
            <div className="container max-w-[18rem] bg-white rounded-3xl dark:bg-gray-800 shadow-lg transform duration-200 easy-in-out m-12">
                <div className="flex justify-center px-5 -mt-12 mb-1">
                    <span className="block relative h-28 w-32">
                        <img alt={name}
                        src={assestIcon[iconLink]}
                        className="mx-auto object-cover rounded-full h-24 w-24 bg-gray-900 p-[0.75rem]" />
                    </span>
                </div>
                <p className="px-7 text-center text-gray-400 mt-2 font-normal text-sm dark:text-gray-400">{name}</p>
                <div className="px-7">
                    <div className="justify-center px-4 py-2 text-sm font-bold cursor-pointer max-w-[16rem] mx-auto mt-4 rounded-full dark:text-gray-300   dark:bg-gray-900 ">
                        <div className="flex ">
                            <div className="flex-initial pr-1 text-end w-[65%]">
                                {price}
                            </div>
                            <div className={`flex-initial pl-3 text-start w-[35%] ${changeAsset === "increment" ? "text-green-600" : "text-red-600"}`}>
                                {percentage}
                            </div>
                        </div>
                    </div>
                    <p className="text-center text-gray-400 mt-2 font-normal text-sm dark:text-gray-400">Price</p>
                </div>
                <div className="px-7 text-center">
                    <div
                    className="justify-center px-4 py-2 font-bold cursor-pointer max-w-min mx-auto mt-2 rounded-full dark:text-gray-300   dark:bg-gray-900 ">
                    {lockedValue}
                    </div>

                    <p className="text-gray-400 mt-2 font-normal text-sm dark:text-gray-400">TVL</p>
                </div>
                <div className="px-7 mb-6 text-center ">
                    <div className="justify-center px-4 py-2 font-bold cursor-pointer max-w-min mx-auto mt-2 rounded-full dark:text-gray-300   dark:bg-gray-900 ">
                        <div className="flex h-[2rem] overflow-x-scroll w-[6rem]">
                            {pairings.map((pairsIcon: any)=>(
                                <img alt={pairsIcon}
                                src={assestIcon[pairsIcon]}
                                className="object-cover rounded-full h-auto w-[2rem] bg-gray-900 p-1" 
                            />
                            ))}
                        </div>
                    </div>
                    <p className="text-gray-400 mt-2 font-normal text-sm dark:text-gray-400">Popular Pairs</p>
                </div>
            </div>
    )
}

export default Cards;
