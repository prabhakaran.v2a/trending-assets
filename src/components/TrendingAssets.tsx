import React, {useEffect,useState} from 'react';
import axios from "axios";
import Cards from './Cards';
import trendLogo from  '../Images/trend.png'

const TrendingAssets = ()=> {
    const [assetData, setAssetData] =  useState<any>([]);
    const [assestIcon, setAssetIcon] = useState<any>([]);
    useEffect(() => {
        loadJson();
      }, []);
    const loadJson = () => {
    const options = {
      method: "GET",
      url: `data/asset.json`,
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };

    axios
      .request(options)
      .then((response) => {
        setAssetData(response.data.assets);
        setAssetIcon(response.data.assets_icon);
      })
      .catch((error) => {
        console.error(error);
      });
  };
    return (
        <div className="dark:bg-gray-900 dark:text-white h-screen">
            <div className="flex pt-24 pl-12 pb-16">
                <img 
                    alt=""
                    src={trendLogo}
                    className="object-cover rounded-full h-[2rem] w-[2rem] bg-gray-900 " 
                /> 
                <span className="font-bold text-sm pl-[1rem] place-self-center">
                    Trending Assets
                </span> 
            </div>
            <div className=" bg-gray-200 dark:bg-gray-900 flex flex-wrap items-center justify-center">
                {assetData.length !== 0 && assetData.map((assets: any)=> (
                    <Cards 
                        key={assets.id}
                        icon={assets.asset}
                        name={assets.name}
                        price={assets.price}
                        assestIcon={assestIcon}
                        iconLink={assets.icon_link}
                        percentage={assets.price_change}
                        changeAsset={assets.price_range}
                        lockedValue={assets.tvl}
                        pairings={assets.popular_pair}
                    />
                ))}
            </div>
        </div>
    )
}

export default TrendingAssets;